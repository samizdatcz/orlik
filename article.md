---
title: "55 let přehrady Orlík: Porovnejte si letecké snímky krajiny před jejím napuštěním se současností"
perex: "Největší z přehrad vltavské kaskády zatopila deset obcí s téměř sedmi stovkami obytných a hospodářských staveb. Hladina vystoupala až k zámku Orlík; původně stál na skále nad hlubokým údolím. Podívejte se, jak dřív vypadal soutok Vltavy s Otavou, a poslechněte si reportáž Československého rozhlasu z 22. prosince 1961, kdy vodohospodáři gigantickou stavbu uvedli do provozu."
description: "Podívejte se, jak dřív vypadal soutok Vltavy s Otavou a poslechněte si reportáž Československého rozhlasu z 22. prosince 1961, kdy komunisté gigantickou stavbu uvedli do provozu."
authors: ["Petr Kočí", "Jan Cibulka"]
published: "22. prosince 2016"
socialimg: https://interaktivni.rozhlas.cz/orlik/media/socialimg.png
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "orlik"
libraries: [jquery, inline-audio]
recommended:
  - link: https://interaktivni.rozhlas.cz/horko-ve-mestech/
    title: Mapa pražského horka: Stromy ochladí okolí o několik stupňů
    perex: Hlavní město hledá cesty, jak se vypořádat s tropickými vedry.
    image: https://interaktivni.rozhlas.cz/horko-ve-mestech/media/socimg.png
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/1428881
    title: Jak se mění česká krajina? Porovnejte si 10 nejzastavěnějších míst
    perex: Už deset procent území České republiky je zastavěno a vybetonováno, konstatuje zpráva o životním prostředí, kterou schválila vláda.
    image: http://media.rozhlas.cz/_obrazek/3263314--priklad-komercni-suburbanizace-obchodni-centrum-letnany--1-300x175p0.png
  - link: https://interaktivni.rozhlas.cz/sudety/
    title: Existují Sudety? Hranice jsou zřetelné i sedmdesát let po vysídlení Němců
    perex: Statistiky nezaměstnanosti, kvality vzdělání nebo volební účasti v Česku dodnes kopírují hranice historických Sudet.
    image: https://samizdat.cz/data/sudety-clanek/www/media/vysidleni.jpg

---

"Téměř k nepoznání se změnila krajina kolem přehradní zdi za poslední dva měsíce. Zmizela úzká stužka koryta Vltavy, zmizely i nejbližší vesnice a voda zaplavila spodní část přehrady. Má už dnes takovou sílu, že může roztočit kolo turbíny," hlásil přesně před 55 lety od nově dokončené přehrady reportér Československého rozhlasu.

## Soudruh náměstek přichází k rozváděcí desce, otáčí kolem...

<aside class="small">
  <h3>
    1961: "Chlouba strojařů" – Spuštění největší Kaplanovy turbíny na světě
  </h3>
  <figure>
    <audio src="https://samizdat.blob.core.windows.net/zvuky/orlik_1960.mp3" controls="controls"></audio>
  </figure>
  <figcaption>
    Poslechněte si reportáž z archivu Českého rozhlasu
  </figcaption>
</aside>

Strojovna s pět metrů vysokou turbínou – jejíž rotující části váží 700 tun – se podle reportáže, kterou si můžete poslechnout v audio přehrávači, při slavnostním spuštění zaplnila funkcionáři a dělníky z přehrady.

Těch na šest let trvající stavbě den co den pracovalo na patnáct set, dva z nich při budování vodního díla zahynuli. I díky tomu se komunistickému režimu podařilo klíčovou stavbu druhé pětiletky dokončit osm měsíců před plánovaným termínem.

Český rozhlas připravil k 55. výročí spuštění elektrárny interaktivní mapu, ve které si můžete porovnat, jak se krajina kolem soutoku Vltavy s Otavou změnila, a podívat se, co všechno pod vodní hladinou zmizelo. Na historických leteckých snímcích je také vidět rozestavěnou hráz, která je po dokončení dlouhá 545 metrů a v koruně dosahuje výšky 91 metrů. 

<div data-bso=1></div>
<aside class="big">
<iframe src="https://samizdat.cz/data/suburbanizace-mapa/www/#49.5951,14.1800,9" class="ig" width="1000" height="600" scrolling="no" style="margin: 10px 0px;" frameborder="0"></iframe>
</aside>

Levou mapu můžete přepnout z leteckého pohledu na klasickou místopisnou mapu, ve které uvidíte místní názvy.

Historickou leteckou mapu poskytla Českému rozhlasu [CENIA, česká informační agentura životního prostředí](http://www1.cenia.cz/www/). Snímky pořídili vojenští geografové [VGHMÚř v Dobrušce](http://www.geoservice.army.cz/htm/s_urad.html). Současná letecká a místopisná mapa pochází ze služeb [Českého úřadu zeměměřického a katastrálního](http://www.cuzk.cz/).

## Nejhlubší a nejobjemnější

<aside class="small">
  <h3>
    1981: Lipno, Slapy, Kamýk, Orlík: Čtyři přehrady, které změnily tvář staré řeky
  </h3>
  <figure>
    <audio src="https://samizdat.blob.core.windows.net/zvuky/orlik_1980.mp3" controls="controls"></audio>
  </figure>
  <figcaption>
    Poslechněte si reportáž z archivu Českého rozhlasu
  </figcaption>
</aside>

Poslední, čtvrtá turbína vodní elektrárny byla spuštěna 10. dubna 1962. Celkový výkon vodní elektrárny činí 364 megawattů, to je přibližně polovina z výkonu celé vltavské kaskády.

Přehrada Orlík je nejhlubší (až 74 metrů) a nejobjemnější (720 tisíc metrů krychových) ze všech vodních děl v České republice. Jezero, které vytvořila, je dlouhé 68 kilometrů.

V příspěvku Československého rozhlasu ke 20. výročí dokončení přehrady, nezazněla jména všech vltavských přehrad: Chyběly Štěchovice a Vrané, dokončené v letech 1945 a 1936 a také nádrže Hněvkovice a Kořensko, zbudované na přelomu 80. a 90. let.  

<aside class="small">
  <h3>
    1983: Hosty vrchního hrázného Zdeňka Zídka u "malé Niagary"
  </h3>
  <figure>
    <audio src="https://samizdat.blob.core.windows.net/zvuky/orlik_1983.mp3" controls="controls"></audio>
  </figure>
  <figcaption>
    Poslechněte si reportáž z archivu Českého rozhlasu
  </figcaption>
</aside>

"Na své cestě Čechami dlouhé 430 kilometrů si klidně zpívej dál svou písničku, stará romantická řeko," rozloučil se s posluchači Československého rozhlasu v roce 1981 redaktor Luboš Lidický. "Bílé hřívy tvých peřejí zpoutala moderní doba. Milion koní na Vltavě přestalo unikat člověku."